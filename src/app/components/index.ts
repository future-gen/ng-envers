export * from './nav-bar/nav-bar.component';
export * from './login/login.component';
export * from './error404/error404.component';
export * from './branch/branch.component';
export * from './institute/institute.component';
