import { Component, OnInit } from '@angular/core';
import { Branch, Institute } from 'src/app/model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BranchService, InstituteService } from 'src/app/services';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.scss']
})
export class BranchComponent implements OnInit {
  constructor(
    private branchService: BranchService,
    private instituteService: InstituteService
  ) { }

  branchs: Branch[];
  cols: any[];

  branchForm: FormGroup;
  name: FormControl;
  institute: FormControl;
  selectedBranch: Branch;
  institutes: Institute[];

  ngOnInit() {
    this.instituteService.getInstitutes().subscribe(resp => {
      this.institutes = [];
      this.institutes.push({
        instituteId: null,
        name: 'Select Institute'
      });
      this.institutes = this.institutes.concat(resp);
      console.log(this.institutes);
    });

    this.name = new FormControl('', Validators.required);
    this.institute = new FormControl('', Validators.required);
    this.branchForm = new FormGroup({
      name: this.name,
      institute: this.institute
    });
    this.cols = [
      { field: 'idx', header: '#' },
      { field: 'name', header: 'Name' }
    ];

    this.branchService.getBranchs().subscribe(resp => {
      this.branchs = resp;
      console.log(this.branchs);
    });
  }

  saveBranch(formValues: any): void {
    if (this.branchForm.valid) {
      const branch: Branch = {
        branchId: this.selectedBranch ? this.selectedBranch.branchId : 0,
        name: formValues.name,
        institute: formValues.institute
      };
      this.branchService.addBranch(branch).subscribe(data => {
        this.branchs.push(data as Branch);
      });
    }
  }

  onRowEditInit(branch: Branch) {
    this.selectedBranch = branch;
  }

  onRowEditSave(branch: Branch) {
    const index = this.branchs.indexOf(branch);
    this.branchService.updateBranch(branch).subscribe(data => {
      this.branchs[index] = data as Branch;
    });
  }

  onRowEditCancel(branch: Branch, index: number) {
    this.branchService.deleteBranch(branch).subscribe(data => {
      this.branchs.splice(index, 1);
    });
  }
}
