import { Component, OnInit } from '@angular/core';
import { InstituteService } from 'src/app/services';
import { Institute } from 'src/app/model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-institute',
  templateUrl: './institute.component.html',
  styleUrls: ['./institute.component.scss']
})
export class InstituteComponent implements OnInit {
  constructor(private instituteService: InstituteService) {}

  institutes: Institute[];
  cols: any[];

  instituteForm: FormGroup;
  name: FormControl;
  selectedInstitute: Institute;
  ngOnInit() {
    this.name = new FormControl('', Validators.required);
    this.instituteForm = new FormGroup({
      name: this.name
    });
    this.cols = [
      { field: 'idx', header: '#' },
      { field: 'name', header: 'Name' }
    ];

    this.instituteService.getInstitutes().subscribe(resp => {
      this.institutes = resp;
      console.log(this.institutes);
    });
  }

  saveInstitute(formValues: any): void {
    if (this.instituteForm.valid) {
      const institute: Institute = {
        instituteId: this.selectedInstitute
          ? this.selectedInstitute.instituteId
          : 0,
        name: formValues.name
      };
      this.instituteService.addInstitute(institute).subscribe(data => {
        this.institutes.push(data as Institute);
      });
    }
  }

  onRowEditInit(institute: Institute) {
    this.selectedInstitute = institute;
  }

  onRowEditSave(institute: Institute) {
    const index = this.institutes.indexOf(institute);
    this.instituteService.updateInstitute(institute).subscribe(data => {
      this.institutes[index] = data as Institute;
    });
  }

  onRowEditCancel(institute: Institute, index: number) {
    this.instituteService.deleteInstitute(institute).subscribe(data => {
      this.institutes.splice(index, 1);
    });
  }
}
