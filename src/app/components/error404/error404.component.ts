import { OnInit, Component } from '@angular/core';
import { AuthenticationService } from 'src/app/services';

@Component({
    selector: 'app-error-404',
    template: `
        <h1>Not Found</h1>
    `
    ,
    styles: ['h1 {color : red}']
})
export class Error404Component implements OnInit {
    constructor(private auth: AuthenticationService) {

    }
    ngOnInit(): void {
    }

}
