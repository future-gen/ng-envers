import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/index';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    emailId: string;
    password: string;
    loginInvalid = false;
    mouseoverLogin = false;
    constructor(private authService: AuthenticationService, private router: Router) {

    }
    ngOnInit(): void {
        this.loginInvalid = false;
    }

    public login(): void {
        this.authService.authenticate(this.emailId, this.password).subscribe(resp => {
            if (!resp) {
                this.loginInvalid = true;
            } else {
                // tslint:disable-next-line: no-string-literal
                this.router.navigate(['/institute']);
            }
        });
    }
}
