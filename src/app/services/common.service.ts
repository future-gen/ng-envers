import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CommonService {

    cloneObject(obj): any {
        const clone = {};
        for (let i in obj) {
            if (obj[i] != null && typeof (obj[i]) === 'object') {
                clone[i] = this.cloneObject(obj[i]);
            } else {
                clone[i] = obj[i];
            }
        }
        return clone;
    }
}
