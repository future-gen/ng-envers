import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private baseUrl = '/api/user';
    constructor(private httpClient: HttpClient) { }

    public authenticate(emailId: string, password: string) {
        const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(emailId + ':' + password) });

        return this.httpClient.get(this.baseUrl + '/validateLogin', { headers })
            .pipe(
                tap(status => {
                    sessionStorage.setItem('emailId', emailId);
                    const authString = 'Basic ' + btoa(emailId + ':' + password);
                    sessionStorage.setItem('basicauth', authString);
                    return status;
                }))
            .pipe(catchError(err => {
                return of(false);
            }));
    }

    public isUserLoggedIn() {
        const user = sessionStorage.getItem('emailId');
        console.log(!(user === null));
        return !(user === null);
    }

    public logOut() {
        sessionStorage.removeItem('emailId');
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
