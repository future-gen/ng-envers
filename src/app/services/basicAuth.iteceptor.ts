import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class BasicAuthHtppInterceptorService implements HttpInterceptor {

    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {

        if (sessionStorage.getItem('emailId') && sessionStorage.getItem('basicauth')) {

            req = req.clone({
                setHeaders: {
                    Authorization: sessionStorage.getItem('basicauth'),
                    'X-Requested-With': 'XMLHttpRequest'
                }
            });
        }

        return next.handle(req);

    }
}
