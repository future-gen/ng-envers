import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Institute } from '../model/index';

@Injectable({
  providedIn: 'root'
})
export class InstituteService {
  private baseUrl = '/api/institute';
  private options = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) {}

  getOptions() {
    // const token = localStorage.getItem('access_token');
    return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }; // .set('Authorization', 'Bearer' + token) }
  }

  public getInstitutes(): Observable<Institute[]> {
    // const token = localStorage.getItem('access_token');
    const options = this.getOptions();
    return this.http.get<Institute[]>(this.baseUrl, options).pipe(
      tap(_ => console.log('fetched heroes')),
      catchError(this.handleError<Institute[]>('getInstitutes', []))
    );
  }

  public getInstituteById(id: number): Observable<Institute> {
    return this.http.get<Institute>(`${this.baseUrl}/${id}`).pipe(
      tap(_ => console.log('fetched heroes')),
      catchError(this.handleError<Institute>('getInstituteById'))
    );
  }

  public updateInstitute(institute: Institute) {
    return this.http
      .put(`/api/institute/${institute.instituteId}`, institute, this.options)
      .pipe(
        tap(data => {
          // this.authService.currentUser = data as Institute;
        })
      )
      .pipe(catchError(this.handleError('deleteHero')));
  }

  public deleteInstitute(institute: Institute) {
    return this.http
      .delete(`/api/institute/${institute.instituteId}`, this.options)
      .pipe(catchError(this.handleError('deleteHero')));
  }

  public addInstitute(institute: Institute) {
    return this.http
      .post('/api/institute/', institute, this.options)
      .pipe(
        tap(data => {
          // this.authService.currentUser = data as Institute;
        })
      )
      .pipe(catchError(this.handleError('deleteHero')));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
