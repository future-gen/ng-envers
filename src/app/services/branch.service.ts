import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Branch } from '../model/index';

@Injectable({
  providedIn: 'root'
})
export class BranchService {
  private baseUrl = '/api/branch';
  private options = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getOptions() {
    // const token = localStorage.getItem('access_token');
    return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }; // .set('Authorization', 'Bearer' + token) }
  }

  public getBranchs(): Observable<Branch[]> {
    // const token = localStorage.getItem('access_token');
    const options = this.getOptions();
    return this.http.get<Branch[]>(this.baseUrl, options).pipe(
      tap(_ => console.log('fetched heroes')),
      catchError(this.handleError<Branch[]>('getBranchs', []))
    );
  }

  public getBranchById(id: number): Observable<Branch> {
    return this.http.get<Branch>(`${this.baseUrl}/${id}`).pipe(
      tap(_ => console.log('fetched heroes')),
      catchError(this.handleError<Branch>('getBranchById'))
    );
  }

  public updateBranch(branch: Branch) {
    return this.http
      .put(`/api/branch/${branch.branchId}`, branch, this.options)
      .pipe(
        tap(data => {
          // this.authService.currentUser = data as Branch;
        })
      )
      .pipe(catchError(this.handleError('deleteHero')));
  }

  public deleteBranch(branch: Branch) {
    return this.http
      .delete(`/api/branch/${branch.branchId}`, this.options)
      .pipe(catchError(this.handleError('deleteHero')));
  }

  public addBranch(branch: Branch) {
    return this.http
      .post('/api/branch/', branch, this.options)
      .pipe(
        tap(data => {
          // this.authService.currentUser = data as Branch;
        })
      )
      .pipe(catchError(this.handleError('deleteHero')));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
