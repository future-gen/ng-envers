import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  LoginComponent,
  InstituteComponent,
  BranchComponent
} from './components/index';
import { AuthGaurdService } from './services';

export const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },

  {
    path: 'institute',
    component: InstituteComponent,
    canActivate: [AuthGaurdService]
  },

  {
    path: 'branch',
    component: BranchComponent,
    canActivate: [AuthGaurdService]
  },

  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
