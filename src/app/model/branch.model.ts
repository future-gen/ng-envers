import { Institute } from './institute.model';

export class Branch {
  branchId: number;
  name: string;
  institute: Institute;
}
