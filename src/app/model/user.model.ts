export class User {
    constructor(
        public status: boolean,
        public emailId: string,
        public firstName: string,
        public lastName: string,
    ) { }

}
